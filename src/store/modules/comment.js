export default {
    state: {
        // Define states
        comments: undefined,
        comment: undefined,
    },
    
    getters: {
        // Define getters
        getComments: (state) => state.comments,
        getComment: (state) => state.comment,
    },

    mutations: {
        // Define mutations
        SET_COMMENTS( state, payload ){ state.comments = payload.data },
        SET_COMMENT( state, payload ){ state.comment = payload.data },
        DELETE_COMMENT(state, comment){ 
            let index = state.comments.findIndex(com => com._id == comment._id)
            state.comments.splice(index, 1)
        }
    },

    actions: {
        fetchCommentList(context){
            fetch( `${process.env.VUE_APP_API_URL}/api/comment`, { method: `GET` }) //=> Fetch API
            .then( response => !response.ok ? console.log(response) : response.json(response)) //=> Check response
            .then( async (apiResponse) => await context.commit('SET_COMMENTS', { data: apiResponse.data })) //=> Commit changes
            .catch( apiError => console.log(apiError)) //=> Catch error
        },

        fetchComment(context, id){
            fetch( `${process.env.VUE_APP_API_URL}/api/comment/${id}`, { method: 'GET' }) //=> Fetch API
            .then( response => !response.ok ? console.log(response) : response.json(response)) //=> Check response
            .then( async (apiResponse) => await context.commit('SET_COMMENT', { data: apiResponse.data })) //=> Commit changes
            .catch( apiError => console.log(apiError)) //=> Catch error
        },

        addComment(context, data){
            fetch( `${process.env.VUE_APP_API_URL}/api/comment`, { 
                method: 'POST',
                body: JSON.stringify({ title: data.title, content: data.content }),
                headers: {
                    'Content-Type': 'application/json'
                },
                credentials: 'include'
            }) 
            .then( response => !response.ok ? console.log(response) : response.json(response)) //=> Check response
            .then( async (apiResponse) => await context.commit('SET_COMMENTS', { data: apiResponse.data })) //=> Commit changes
            .catch( apiError => console.log(apiError)) //=> Catch error
        },
        
        deleteComment(context, id){
            fetch( `${process.env.VUE_APP_API_URL}/api/comment/${id}`, { 
                method: 'DELETE',
                body: JSON.stringify({ id : id }),
                headers: {
                    'Content-Type': 'application/json'
                },
                credentials: 'include'
            }) 
            .then( response => !response.ok ? console.log(response) : response.json(response)) //=> Check response
            .then( async (apiResponse) => await context.commit('DELETE_COMMENT', { data: apiResponse.data })) //=> Commit changes
            .catch( apiError => console.log(apiError)) //=> Catch error
        }
    }
}