export default {
    state: {
        // Define states
        posts: undefined,
        post: undefined,
    },
    
    getters: {
        // Define getters
        getPosts: (state) => state.posts,
        getPost: (state) => state.post,
    },

    mutations: {
        // Define mutations
        SET_POSTS( state, payload ){ state.posts = payload.data },
        SET_POST( state, payload ){ state.post = payload.data },
    },

    actions: {
        // [CRUD] GET Method to get post list
        fetchPostList(context){
            fetch( `${process.env.VUE_APP_API_URL}/api/post`, { method: `GET` }) //=> Fetch API
            .then( response => !response.ok ? console.log(response) : response.json(response)) //=> Check response
            .then( async (apiResponse) => await context.commit('SET_POSTS', { data: apiResponse.data })) //=> Commit changes
            .catch( apiError => console.log(apiError)) //=> Catch error
        },

        //[CRUD] GET Method to get post data from ID
        fetchSinglePost(context, id){
            fetch( `${process.env.VUE_APP_API_URL}/api/post/${id}`, { method: 'GET' }) //=> Fetch API
            .then( response => !response.ok ? console.log(response) : response.json(response)) //=> Check response
            .then( async (apiResponse) => await context.commit(`SET_POST`, { data: apiResponse.data })) //=> Commit changes
            .catch( apiError => console.log(apiError)) //=> Catch error
        },

         //[CRUD] Create Post
        addPost(context, data){
            fetch( `${process.env.VUE_APP_API_URL}/api/post`, { 
                method: 'POST',
                body: JSON.stringify({ headline: data.headline, body: data.body }),
                headers: {
                    'Content-Type': 'application/json'
                },
                credentials: 'include'
            }) 
            .then(resp => console.log(resp))
            .then( response => !response.ok ? console.log(response) : response.json(response)) //=> Check response
            .then( async (apiResponse) => await context.commit('SET_POSTS', { data: apiResponse.data })) //=> Commit changes
            .catch( apiError => console.log(apiError)) //=> Catch error
        }
    }
}